# abiyo-leafs

![License] ![ci-passing] ![rust-version]

`abiyo-leafs` is part of the `abiyo` project which strives to bring together hydroponics, IoT and AI.

`abiyo-leafs` itself is a collection of services that make physical sensors and actuators available through MQTT. The motivation behind is learning rust.

## Overview

Currently following sensors are covered:

- Water conductivity sensor (I2C interface): [CN0349]
- Water pH sensor (SPI interface): [CN0326]
- Water level sensor (UART and GPIO interface): [Sonic]

Further a relay board for switching mains power is supported:

- Relay module with 16 relays (USB serial interface): [Relay]

Together those make up a basic hydroponic setup focusing on irrigation and nutrient supply.

This repo does not provide means for packaging, publishing, deployment and running of the services. These topics should be covered independently of building the actual service binaries.

## Structure

The services included in this repo are kept as separate binaries for each hardware component for simplicity. Combining all these essential services into one single executable make sense from the perspective of deployment but requires slightly more advanced topics like threading or asynchronous support.

## Configuration

Everything that needs to be known before starting the service

- Command line options
- Use ENV variables
- Hardcoded default fallback

Credentials need to be dealt with differently. Putting credentials into ENV variables or command line options is discouraged since they can leak easily. 

- Inject credentials via file/stdin/socket

The beauty of using a file is that one can pipe credentials from a key-management tool directly into the service.

## Developing

A `devcontainer.json` is available for use with `vscode`. This enables development inside a remote container. The devcontainer is using a dockerized toolchain available through `cr.grahsl.net/rust-aarch64-dev:0.2.0` image from the [rust-build-agent].

To get started open vscode and issue `Remote-Containers: Clone repository into Container Volume` and point to the gitlab URL of this project. After some initial setup and building dependencies, this provide a very convenient workflow including a full featured debugger.

## Building overview

There are several ways of building:

- building locally using the rust toolchain on the host
- building using dockerized toolchain
- building using the gitlab CI

### Building locally from source

Building is trivially done by using the `cargo` command.

```bash
cargo build
```

### Building using a dockerized environment

Instead of installing dependencies directly to your host, you can use the `cr.grahsl.net/rust-aarch64-build:0.2.0` image from the [rust-build-agent] repository for building and testing.

The `Makefile` provides a convencience wrapper to the `docker-compose.yaml` which makes use of the dockerized build env. The repository root folder is mounted as a volume at `/src` inside the container.

To build using the dockerized toolchain:

```bash
make build
```

To drop into a shell containing some further dev-tools issue:

```bash
make shell
```

*Note*: To avoid fetching and building all dependencies on every start of the container, the `docker-compose.yaml` sets the `CARGO_HOME` env variable to locate
the cache for `cargo` in the folder `.cargo` inside the repository root folder.

### Building using the gitlab CI

The CI configuration for Gitlab `.gitlab-ci.yml` contains two stages: `build` and `test`.

`gitlab-ci.yml` is setup to require a runner tagged `gstrust`.

A compatible runner image is `cr.grahsl.net/rust-aarch64-build:0.2.0` from [rust-build-agent].

#### Cache

To avoid downloading dependencies for each job, a cache can be configured in `gitlab-ci.yml` by defining globally:

```
cache:
  key: gst-rust-playground
  paths:
   - cargo
   - target

variables:
  CARGO_HOME: "${CI_PROJECT_DIR}/cargo"
  CARGO_TARGET_DIR: "${CI_PROJECT_DIR}/target"
```

This tells the gitlab-runner to take the `cargo` and `target` subfolders (relative to the `CI_PROJECT_DIR`) to move it (compressed) to the cache volume. On each job, the cache is picked from the volume, extracted into the `CI_PROJECT_DIR` and stored back after the job has finished.

The `variables:` are read by `cargo` set the locations explicitly since they are used by the `cache:` section.

*TODO*  do not handle `target` folder as cache but as an artifact.

#### Speedup CI builds

For large amounts of dependencies and build artifacts, this procedure is very slow.

As a workaround, one can set the `cargo` and `target` folders to reside directly on the cache volume and exclude it from being handled as cache by gitlab-runner.

This is accomplished by instructing `cargo` to use custom locations:

```
cache:
  key: gst-rust-playground

variables:
  CARGO_HOME: "/cache/cargo"
  CARGO_TARGET_DIR: "/cache/target"
```

*Note*: learing the cache is not possible via gitlab or gitlab-runner. 

### Brainstorming section for missing parts

#### Packaging

- Package the binaries using `cargo` into debian packages
- Package with `cargo` into a crate
- Package into a docker image

#### Publishing

- Publish to a private apt server
- Publish to a private container registry
- Publish to `crates.io`

#### Deployment

Several general options can be considered for _getting_ the binaries there:

- Deploy to the host using a `bitbake` recipe when building a poky based distribution
- Deploy using debian packages which are packaged by `cargo`
- Deploy a `docker-compose.yaml` which references published docker images or builds images for each service
- Deploy using `cargo install`

#### Running

For _running_ the services one could use:

- systemd
- docker-compose

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.


[License]: https://img.shields.io/badge/license-MIT-brightgreen
[rust-version]: https://img.shields.io/badge/rust-1.43.0-yellowgreen
[ci-passing]: https://gitlab.com/jgrahsl/abiyo-leafs/badges/master/pipeline.svg
[rust-build-agent]: https://gitlab.com/jgrahsl/rust-build-agent

[CN0349]: https://www.analog.com/en/design-center/reference-designs/circuits-from-the-lab/cn0349.html
[CN0326]: https://www.analog.com/en/design-center/reference-designs/circuits-from-the-lab/cn0326.html
[Relais]: https://www.ebay.at/itm/DC-7V-38V-10A-250V-16-Channel-Serial-Relay-Module-USB-Power-Supply-for-Lights/402528113288?hash=item5db88b9288:g:ToEAAOSwzHxfn97e
[Sonic]: https://eckstein-shop.de/Audiowell-Ultrasonic-Liquid-Level-Sensor-Distance-Rangefinder-Obstacle-Avoidance-Module-UM0034_1