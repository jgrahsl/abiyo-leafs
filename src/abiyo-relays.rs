#![allow(dead_code)]

// This is a poll driver that provides a push interface while honouring hysteresis of an averaged value

extern crate serial;
extern crate ascii;

extern crate structopt;
extern crate pretty_env_logger;

use std::path::PathBuf;
use structopt::StructOpt;

#[path = "drivers/relay.rs"]
mod relay;

use std::str;
use std::time::{Duration};

use serial::prelude::*;

use rumqtt::{MqttClient, MqttOptions, QoS, Notification};
use serial::{SystemPort};

use relay::send;

// MQTT basics
const MQTT_TOPIC : &str = "abiyo/actor/relais";
const MQTT_SERVER_IP : &str = "192.168.0.80";
const MQTT_SERVER_PORT : u16 = 1883;
const MQTT_CLIENT_ID : &str = "abiyo-actor-relais";

fn dump(data : &Vec<u8>) {
    print!("-> dump: ");
    for i in data {

        match ascii::AsciiChar::from_ascii(*i) {
            Ok(ch) => print!("{}", ch),
            Err(_) => print!("_0x{:02x}", *i)
        };
    }

    println!("");
}

fn setup_serial_port(dev : &PathBuf) -> SystemPort {
    let mut port = serial::open(dev).unwrap();

    port.reconfigure(&|settings| {
        settings.set_baud_rate(serial::Baud9600)?;
        settings.set_char_size(serial::Bits8);
        settings.set_parity(serial::ParityNone);
        settings.set_stop_bits(serial::Stop1);
        settings.set_flow_control(serial::FlowNone);
        Ok(())
    }).unwrap();

    port.set_timeout(Duration::from_millis(1000)).unwrap();
    
    return port;
}

fn main() {
    pretty_env_logger::init();
    let opt = Opt::from_args();

    let mqtt_options = MqttOptions::new(&opt.mqtt_client_id, &opt.mqtt_broker_ip, opt.mqtt_broker_port);
    let (mut mqtt_client, notifications) = MqttClient::start(mqtt_options).unwrap();

    let mut port = setup_serial_port(&opt.usb_device); 

    for index in 0..16 {
        let s = format!("{}/{}", &opt.mqtt_topic, index);
        println!("{}", s);
        mqtt_client.subscribe(&s, QoS::AtLeastOnce).unwrap();
    }

    for notification in notifications {
        match notification {
            Notification::Publish(publish) => {
                println!("Publish {:?} {:?}", &publish.topic_name, &publish.payload);
                let mut parts = publish.topic_name.split("/");

                // Bad: hardcoded parsing
                let _ns = parts.next();
                let _category = parts.next();
                let _type = parts.next();
                let relais_num = parts.next().unwrap().parse::<u8>().unwrap();

                let state = match &(publish.payload)[..] {
                    b"0" => false,
                    b"1" => true,
                    _ => { println!("Unknown payload {}", str::from_utf8(&publish.payload).unwrap()); break}
                };

                send(&mut port, relais_num, state);

            },
            Notification::PubAck(id) => {println!("PubAck {:?}", id)},
            Notification::PubRec(id) => {println!("PubRec {:?}", id)},
            Notification::PubRel(id) => {println!("PubRel {:?}", id)},
            Notification::PubComp(id) => {println!("PubComp {:?}", id)},
            Notification::SubAck(id) => {println!("SubAck {:?}", id)},
            _l => {
                println!("something else <{:?}>",_l);}

        }
    }
}

#[derive(StructOpt, Debug)]
#[structopt(name = "basic")]
pub struct Opt {
    // A flag, true if used in the command line. Note doc comment will
    // be used for the help message of the flag. The name of the
    // argument will be, by default, based on the name of the field.
    /// Activate debug mode
    /// 
    // #[structopt(short, long)]
    // debug: bool,

    // The number of occurrences of the `v/verbose` flag
    // Verbose mode (-v, -vv, -vvv, etc.)
    #[structopt(short = "v", long, parse(from_occurrences))]
    verbose: u8,

    // Set speed
    // 
    // #[structopt(short, long, default_value = "42")]
    //speed: f64,

    // Output file
    // 
    // #[structopt(short, long, parse(from_os_str))]
    // output: PathBuf,

    // the long option will be translated by default to kebab case,
    // i.e. `--nb-cars`.
    // Number of cars
    // 
    // #[structopt(short = "c", long)]
    // nb_cars: Option<i32>,

    // admin_level to consider
    // 
    // #[structopt(short, long)]
    // level: Vec<String>,

    // Files to process
    // #[structopt(name = "FILE", parse(from_os_str))]
    // files: Vec<PathBuf>,

    #[structopt(short = "d", env, long, parse(from_os_str), default_value = "/dev/ttyUSB0")]
    pub usb_device: PathBuf,

    #[structopt(short = "b", env, long, default_value = "192.168.0.80")]
    pub mqtt_broker_ip: String,

    #[structopt(short = "p", env, long, default_value = "1883")]
    pub mqtt_broker_port : u16,

    #[structopt(short = "t", env, long, default_value = "abiyo/actor/relais")]
    pub mqtt_topic: String,

    #[structopt(short = "i", env, long, default_value = "abiyo-actor-relais")]
    pub mqtt_client_id : String,
}