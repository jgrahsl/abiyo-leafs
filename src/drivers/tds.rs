use serde::{Deserialize, Serialize};
use i2cdev::linux::{LinuxI2CError, LinuxI2CBus};

#[path = "cn0349.rs"]
pub mod cn0349;
use cn0349::{MeasureContext, SwitchMapping, CalibrationPoint, Cn0349};

#[path = "ad5934.rs"]
pub mod ad5934;

#[path = "adg715.rs"]
pub mod adg715;


fn measure_calibrated_resistance_of_temperature_probe(dev : &cn0349::Cn0349, bus : &mut LinuxI2CBus) -> Result<f64, i2cdev::linux::LinuxI2CError> {
    // This encapsulates probe specifics and ADC calibration
    // The probe specifics govern how we calibrate the measurement system

    // The context needs to be adjusted to the probe
    let ctx = MeasureContext {
        range: ad5934::ControlRegRange::MV200,
        gain: ad5934::ControlRegPGAGain::X5,
        feedback: SwitchMapping::Rfb1k,
        start_frq:  0x418937,
        frq_inc: 0x418937/16,
        num_inc: 1,
        settling_time: 16
    };

    let calib_points : [CalibrationPoint; 2] = [
        CalibrationPoint {config : SwitchMapping::R10k, expected : 0.0001},
        CalibrationPoint {config : SwitchMapping::R1k, expected : 0.001}
    ];
    
    let raw_measurement = dev.calibrate_and_measure_channel_conductivity(bus, &ctx, &calib_points, SwitchMapping::Rtd);

    // Convert to resistance
    let measurement = 1.0 / raw_measurement;

    Ok(measurement)
}

fn measure_calibrated_conductivity_of_ec_probe(dev : &Cn0349, bus : &mut LinuxI2CBus) -> Result<f64, i2cdev::linux::LinuxI2CError> {
    // This encapsulates probe specifics and ADC calibration
    // The probe specifics govern how we calibrate the measurement system


    // Probe type is MeasureContext and CalibrationPoints
    let ctx = MeasureContext {
        range: ad5934::ControlRegRange::MV200,
        gain: ad5934::ControlRegPGAGain::X5,
        feedback: SwitchMapping::Rfb1k,
        start_frq:  0x418937,
        frq_inc: 0x418937/16,
        num_inc: 1,
        settling_time: 16
    };

    let calib_points : [CalibrationPoint; 2] = [
        CalibrationPoint {config : SwitchMapping::R10k, expected : 0.0001},
        CalibrationPoint {config : SwitchMapping::R1k, expected : 0.001}
    ];
    
    let measurement = dev.calibrate_and_measure_channel_conductivity(bus, &ctx, &calib_points, SwitchMapping::Y);

    Ok(measurement)
}


#[builder(default)]
#[derive(derive_builder::Builder, Debug, Serialize, Deserialize)]
pub struct ProbeCalibration {
    ec_solution_temperature_coefficient : f64,
    ec_scale : f64,
    ec_temperature_while_scale_calibration : f64,
    temp_a : f64,
    temp_b : f64,
    temp_c : f64
}

impl Default for ProbeCalibration {
    fn default() -> Self {
        Self {
            ec_solution_temperature_coefficient : 0.0204, // Mostly around 2%
            ec_scale : 2.275, // Trim the output to match the reference solution nominal mS
            ec_temperature_while_scale_calibration : 20.0, // Note the temperature of the solution during calibration of scale

            // let celsius = pc.temp_a * (resistance + pc.temp_b).ln() + pc.temp_c;
            temp_a : -24.0,
            temp_b : 312.9138212,
            temp_c : 213.3720658
        }
    }
}

pub fn measure_compensated_tds_and_temperature(bus : &mut LinuxI2CBus, pc : &ProbeCalibration, dev : &cn0349::Cn0349) -> Result<(f64, f64, f64, f64), i2cdev::linux::LinuxI2CError> {
    //    println!("DDD {:?}", serde_json::to_string(&pc).unwrap());
    /*
    I want to know the mS the solution would have at 25 degrees.
    I can measure the actual mS and the temperature.
    I know that the mS increases by 2% for each degree celsius increase
    I calibrate the probe with a solution that is know to be 1.43mS at 25 degrees celsius
    Would the solution be measured while being 25C, no compensation takes place and the scale factor is used to calibrate the probe
    */

    let resistance = measure_calibrated_resistance_of_temperature_probe(dev, bus)?;
    let cond = measure_calibrated_conductivity_of_ec_probe(dev, bus)? * 1_000.0; // convert from Siemens to milliSiemens

    let celsius = pc.temp_a * (resistance + pc.temp_b).ln() + pc.temp_c;
    
    let ms = cond * (1.0 + ((pc.ec_temperature_while_scale_calibration - celsius) * pc.ec_solution_temperature_coefficient)) * pc.ec_scale;

    Ok((ms, celsius, cond, resistance))
}

// Layers

// ADxxx, encapsulate per node communication details
// Talk to the registers (reflect communication statemachine)

// Boardlevel, orchestrate nodes
// Use registers to perform a measurement sequence (reflect behavior statemachine inside chip)

// cn0349, make use of design as a calibrateable system (presence of measureable reference values)
// Use measurements together with measurement system information to construct a calibrated measurement system (reflect measurement system setup)

// EC, introduce probe specifics and parametrise the cn0349 calibration

// TDS, make use of design as a system that compensates for temperature to proxy TDS (presence of sensors that are design to help model the system with respect to nutient levels)
// Use calibrated measurements together with plant information to compensate sensor reading to compensate for sensor interrelations (reflect plant setup)
