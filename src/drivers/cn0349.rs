use serde::{Serialize, Deserialize};
use i2cdev::linux::{LinuxI2CBus, LinuxI2CMessage, LinuxI2CError};

#[path = "ad5934.rs"]
use super::ad5934;

#[path = "adg715.rs"]
use super::adg715;

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum SwitchMapping {
    Rfb100, // = adg715::Switch::S1 as u8,
    Rfb1k,
    R100,
    R1k,
    R10k,
    Rtd,
    Y
}

impl Into<adg715::Switch> for SwitchMapping {
    fn into(self) -> adg715::Switch {
        match self {
            SwitchMapping::Rfb100 => adg715::Switch::S1,
            SwitchMapping::Rfb1k => adg715::Switch::S2,
            SwitchMapping::R100 => adg715::Switch::S4,
            SwitchMapping::R1k => adg715::Switch::S5,
            SwitchMapping::R10k => adg715::Switch::S6,
            SwitchMapping::Rtd => adg715::Switch::S7,
            SwitchMapping::Y => adg715::Switch::S8
        }
    }
}

#[builder(default)]
#[derive(Clone, Copy)]
#[derive(derive_builder::Builder, Debug, Serialize, Deserialize)]
pub struct MeasureContext {
    pub range: ad5934::ControlRegRange,
    pub gain: ad5934::ControlRegPGAGain,
    pub feedback: SwitchMapping,
    pub start_frq: u32,
    pub frq_inc: u32,
    pub num_inc: u16,
    pub settling_time: u16
}

impl Default for MeasureContext {
    fn default() -> MeasureContext{
        MeasureContext {
            range: ad5934::ControlRegRange::MV200,
            gain: ad5934::ControlRegPGAGain::X5,
            feedback: SwitchMapping::Rfb1k,
            start_frq:  0x418937,
            frq_inc: 0x418937/16,
            num_inc: 1,
            settling_time: 16
        }
    }

}

#[derive(Clone, Copy)]
pub struct CalibrationPoint {
    pub config : SwitchMapping,
    pub expected : f64,
}

#[derive(Clone, Copy)]
struct CalibrationWorkspace {
    pub gf : f64, // Gain
    pub nos : f64, // Zero point
}


pub struct Cn0349 {
    adg715 : adg715::Adg715,
    ad5934 : ad5934::Ad5934
}

// Idea here is that Cn0349 impl is able to generate and apply a calibration as well as owns the subdevices
impl Cn0349 {
    pub fn new(adg715_ : adg715::Adg715, ad5934_ : ad5934::Ad5934) -> Cn0349 {
        Cn0349 {
            adg715 : adg715_,
            ad5934 : ad5934_
        }
    }

    fn measure_raw(&self, bus : &mut LinuxI2CBus, ctx: &MeasureContext, channel: SwitchMapping) -> Result<f64,  i2cdev::linux::LinuxI2CError> {
        // This encapsulates the switch and ADC playing together and the STM of the ADC

        let f : adg715::Switch = ctx.feedback.into();
        let c : adg715::Switch = channel.into();

        self.adg715.set_switch(bus, f as u8 | c as u8)?;
        
        self.ad5934.set_start_frq(bus, ctx.start_frq)?;
        self.ad5934.set_frq_inc(bus, ctx.frq_inc)?;
        self.ad5934.set_num_inc(bus, ctx.num_inc)?;
        self.ad5934.set_settling_time(bus, ctx.settling_time)?;

        self.ad5934.set_control(bus, ad5934::ControlRegMode::Standby, ctx.range, ctx.gain)?;
        self.ad5934.set_control(bus, ad5934::ControlRegMode::InitStartFrq, ctx.range, ctx.gain)?;
        self.ad5934.set_control(bus, ad5934::ControlRegMode::StartSweep, ctx.range, ctx.gain)?;
        
        loop {
            if self.ad5934.get_status_data_is_valid(bus)? 
            {
                break;
            }
        }

        self.ad5934.read_mag(bus)
    }

    fn do_calibration(&self, bus : &mut LinuxI2CBus, ctx : &MeasureContext, points : &[CalibrationPoint; 2]) -> Result<CalibrationWorkspace, LinuxI2CError> {
        
        let measured : [f64; 2] = [
            self.measure_raw(bus, &ctx, points[0].config)?,
            self.measure_raw(bus, &ctx, points[1].config)?
        ];

        let gf = (points[1].expected - points[0].expected) / (measured[1] - measured[0]);
        let nos = measured[0] - (points[0].expected / gf);
                
        // Calculate calibration data
        Ok(CalibrationWorkspace {
            gf : gf,
            nos : nos
        })
    }

    fn apply_calibration(&self, calib : &CalibrationWorkspace, y : f64) -> f64 {
        (y - calib.nos) * calib.gf
    }

    pub fn calibrate_and_measure_channel_conductivity(&self, bus : &mut LinuxI2CBus, ctx : &MeasureContext, points : &[CalibrationPoint; 2], channel : SwitchMapping) -> f64 {
        let calibration = self.do_calibration(bus, ctx, points).unwrap();

        let y = self.measure_raw(bus, &ctx, channel).unwrap();
        
        self.apply_calibration(&calibration, y)
    }
}
