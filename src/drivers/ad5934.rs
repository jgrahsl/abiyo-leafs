use i2cdev::core::{I2CMessage, I2CTransfer};
use i2cdev::linux::{LinuxI2CBus, LinuxI2CMessage};
use serde::{Serialize, Deserialize};

#[derive(Clone, Copy)]
#[repr(u8)]
enum Regs {
    Control16 = 0x80,
    StartFrq24 = 0x82,
    FrqInc24 = 0x85,
    NumInc16 = 0x88,
    SettleTime16 = 0x8A,
    Status = 0x8F,
    Real16 = 0x94,
    Imaginary16 = 0x96
}

#[derive(Clone, Copy)]
#[repr(u16)]
pub enum ControlRegMode {
    Nop1 = 0x0,
    InitStartFrq = 0x1,
    StartSweep = 0x2,
    IncFrq = 0x3,
    RepFrq = 0x4,
    Nop2 = 0x8,
    Nop3 = 0x9,
    Powerdown = 0xA,
    Standby = 0xB,
    Nop4 = 0xC,
    Nop5 = 0xD
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
#[repr(u16)]
pub enum ControlRegRange {
    V2 = 0x0,
    MV200 = 0x1,
    MV400 = 0x2,
    V1 = 0x3
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
#[repr(u16)]
pub enum ControlRegPGAGain {
    X1 = 0x1,
    X5 = 0x0
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
#[repr(u8)]
enum StatusRegMasks{
    DataValid = 0x2,
    SweepComplete = 0x4
}

pub struct Ad5934 {
    i2c_adr : u16
}

impl Ad5934 {
    pub fn new() -> Ad5934 {
        Ad5934 {
            i2c_adr : Self::I2C_ADDR
        }
    }

    const I2C_ADDR : u16 = 0x0D;

    const CONTROL_REG_MODE_OFFSET : u8 = 12;    
    const CONTROL_REG_RANGE_OFFSET : u8 = 9;
    const CONTROL_REG_PGAGAIN_OFFSET : u8 = 8;

    const BLOCK_WRITE : u8 = 0b10100000;
    const BLOCK_READ : u8 =  0b10100001;
    const REG_CMD : u8 =     0b10110000;

    fn read_reg8_val8(&self, bus : &mut LinuxI2CBus, reg : u8) -> Result<u8, i2cdev::linux::LinuxI2CError> {
        let reg_command  : [u8; 2]= [Self::REG_CMD, reg];
        let block_read : [u8; 2] = [Self::BLOCK_READ, 1];
        let mut data : [u8; 1] = [0; 1];

        let mut msgs : [LinuxI2CMessage; 3] = [
            LinuxI2CMessage::write(&reg_command).with_address(Self::I2C_ADDR),
            LinuxI2CMessage::write(&block_read).with_address(Self::I2C_ADDR),
            LinuxI2CMessage::read(&mut data).with_address(Self::I2C_ADDR)
        ];
    
        bus.transfer(&mut msgs)?;

        return Ok(data[0]);
    }

    fn read_reg8_val16(&self, bus : &mut LinuxI2CBus, reg : u8) -> Result<u16, i2cdev::linux::LinuxI2CError> {
        let value : u16;
        let reg_command  : [u8; 2] = [Self::REG_CMD, reg];
        let block_read : [u8; 2] = [Self::BLOCK_READ, 2];
        let mut data : [u8; 2] = [0; 2];

        let mut msgs : [LinuxI2CMessage; 3] = [
            LinuxI2CMessage::write(&reg_command).with_address(self.i2c_adr),
            LinuxI2CMessage::write(&block_read).with_address(self.i2c_adr),
            LinuxI2CMessage::read(&mut data).with_address(self.i2c_adr)
        ];
    
        bus.transfer(&mut msgs)?;
        
        value = ((data[0] as u16) << 8) | data[1] as u16;
        return Ok(value);
    }

    fn read_reg8_val24(&self, bus : &mut LinuxI2CBus, reg : u8) -> Result<u32, i2cdev::linux::LinuxI2CError> {
        let value : u32;
        let reg_command  : [u8; 2]= [Self::REG_CMD, reg];
        let block_read : [u8; 2] = [Self::BLOCK_READ, 3];
        let mut data : [u8; 3] = [0; 3];

        let mut msgs : [LinuxI2CMessage; 3] = [
            LinuxI2CMessage::write(&reg_command).with_address(self.i2c_adr),
            LinuxI2CMessage::write(&block_read).with_address(self.i2c_adr),
            LinuxI2CMessage::read(&mut data).with_address(self.i2c_adr)
        ];
    
        bus.transfer(&mut msgs)?;
        
        value = ((data[0] as u32) << 16) | ((data[1] as u32) << 8) | data[2] as u32;
        return Ok(value);
    }

    fn write_reg8_val8(&self, bus : &mut LinuxI2CBus, reg : u8, val : u8) -> Result<(), i2cdev::linux::LinuxI2CError> {
        let reg_command  : [u8; 2]= [Self::REG_CMD, reg];
        let block_write : [u8; 3] = [Self::BLOCK_WRITE,
                                1,
                                val];

        let mut msgs : [LinuxI2CMessage; 2] = [
            LinuxI2CMessage::write(&reg_command).with_address(self.i2c_adr),
            LinuxI2CMessage::write(&block_write).with_address(self.i2c_adr),
        ];
    
        bus.transfer(&mut msgs)?;
        
        return Ok(());
    }

    
    fn write_reg8_val16(&self, bus : &mut LinuxI2CBus, reg : u8, val : u16) -> Result<(), i2cdev::linux::LinuxI2CError> {
        let reg_command  : [u8; 2]= [Self::REG_CMD, reg];
        let block_write : [u8; 4] = [Self::BLOCK_WRITE,
                                2,
                                ((val >> 8) & 0xff) as u8,
                                ((val >> 0) & 0xff) as u8];

        let mut msgs : [LinuxI2CMessage; 2] = [
            LinuxI2CMessage::write(&reg_command).with_address(self.i2c_adr),
            LinuxI2CMessage::write(&block_write).with_address(self.i2c_adr),
        ];
    
        bus.transfer(&mut msgs)?;
        
        return Ok(());
    }

    fn write_reg8_val24(&self, bus : &mut LinuxI2CBus, reg : u8, val : u32) -> Result<(), i2cdev::linux::LinuxI2CError> {
        let reg_command  : [u8; 2]= [Self::REG_CMD, reg];
        let block_write : [u8; 5] = [Self::BLOCK_WRITE,
                                3,
                                ((val >> 16) & 0xff) as u8,
                                ((val >> 8) & 0xff) as u8,
                                ((val >> 0) & 0xff) as u8];

        let mut msgs : [LinuxI2CMessage; 2] = [
            LinuxI2CMessage::write(&reg_command).with_address(self.i2c_adr),
            LinuxI2CMessage::write(&block_write).with_address(self.i2c_adr),
        ];
    
        bus.transfer(&mut msgs)?;
        
        return Ok(());
    }  
    
    //// Public
    /// 
    pub fn set_start_frq(&self, bus : &mut LinuxI2CBus, frq : u32) -> Result<(), i2cdev::linux::LinuxI2CError> { return self.write_reg8_val24(bus, Regs::StartFrq24 as u8, frq); }

    pub fn set_frq_inc(&self, bus : &mut LinuxI2CBus, inc : u32) -> Result<(), i2cdev::linux::LinuxI2CError>  { return self.write_reg8_val24(bus, Regs::FrqInc24 as u8, inc); }

    pub fn set_num_inc(&self, bus : &mut LinuxI2CBus, num : u16) -> Result<(), i2cdev::linux::LinuxI2CError>  { return self.write_reg8_val16(bus, Regs::NumInc16 as u8, num); }

    pub fn set_settling_time(&self, bus : &mut LinuxI2CBus, time : u16) -> Result<(), i2cdev::linux::LinuxI2CError>  { return self.write_reg8_val16(bus, Regs::SettleTime16 as u8, time & 0x1ff); }

    pub fn set_control(&self, bus : &mut LinuxI2CBus, mode : ControlRegMode, range : ControlRegRange, gain : ControlRegPGAGain) -> Result<(), i2cdev::linux::LinuxI2CError> {
        // Assume internal clock

        let val : u16 = ((mode as u16) << Self::CONTROL_REG_MODE_OFFSET) | ((range as u16) << Self::CONTROL_REG_RANGE_OFFSET) | ((gain as u16) << Self::CONTROL_REG_PGAGAIN_OFFSET);
        return self.write_reg8_val16(bus, Regs::Control16 as u8, val);
    }

    pub fn get_status_data_is_valid(&self, bus : &mut LinuxI2CBus) -> Result<bool, i2cdev::linux::LinuxI2CError> {
        let status : u8 = self.read_reg8_val8(bus, Regs::Status as u8)?;

        if (status & (StatusRegMasks::DataValid as u8)) != 0 {
            return Ok(true);
        }

        return Ok(false);
    }

    pub fn get_status_sweep_is_complete(&self, bus : &mut LinuxI2CBus) -> Result<bool, i2cdev::linux::LinuxI2CError> {
        let status : u8 = self.read_reg8_val8(bus, Regs::Status as u8)?;

        if (status & (StatusRegMasks::SweepComplete as u8)) != 0 {
            return Ok(true);
        }

        return Ok(false);
    }

    pub fn get_real_data(&self, bus : &mut LinuxI2CBus) -> Result<u16, i2cdev::linux::LinuxI2CError> {
        let value : u16 = self.read_reg8_val16(bus, Regs::Real16 as u8)?;
        return Ok(value);
    }

    pub fn get_imaginary_data(&self, bus : &mut LinuxI2CBus) -> Result<u16, i2cdev::linux::LinuxI2CError> {
        let value : u16 = self.read_reg8_val16(bus, Regs::Imaginary16 as u8)?;
        return Ok(value);
    }

    pub fn read_mag(&self, bus : &mut LinuxI2CBus) -> Result<f64, i2cdev::linux::LinuxI2CError> {
        let ru = self.get_real_data(bus)?;
        let iu = self.get_imaginary_data(bus)?;

        let r : i16 = ru as i16;
        let i : i16 = iu as i16;

        return Ok(((r as f64)*(r as f64) + (i as f64)*(i as f64)).sqrt());
    }
}
