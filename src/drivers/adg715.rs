use i2cdev::core::{I2CMessage, I2CTransfer};
use i2cdev::linux::{LinuxI2CBus, LinuxI2CMessage, LinuxI2CError};
use serde::{Serialize, Deserialize};

#[derive(Clone, Copy)] 
#[repr(u8)]
pub enum Switch {
    S1 = 0x01,
    S2 = 0x02,
    S3 = 0x04,
    S4 = 0x08,
    S5 = 0x10,
    S6 = 0x20,
    S7 = 0x40,
    S8 = 0x80                
}

impl Into<u8> for Switch {
    fn into(self) -> u8 {
        self as u8
    }
}


// #[builder(default)]
// #[derive(derive_builder::Builder, Debug, Serialize, Deserialize)]

#[derive(Debug, Serialize, Deserialize)]
pub struct ProbeCalibration {
    ec_solution_temperature_coefficient : f64,
    ec_scale : f64,
    ec_temperature_while_scale_calibration : f64,
    temp_a : f64,
    temp_b : f64,
    temp_c : f64
}

pub struct Adg715 {
    i2c_addr : u16
    //    i2c_addr : u16 = 0x48;
    //i2c_bus : &str = "/dev/i2c-1";
}

impl Adg715 {
    pub fn new() -> Self {
        Adg715 {
            i2c_addr : 0x48
        }
    }

    fn write_val8(&self, bus : &mut LinuxI2CBus, value : u8) -> Result<(), LinuxI2CError> {
        let payload  : [u8; 1]= [value];

        let mut msgs : [LinuxI2CMessage; 1] = [
            LinuxI2CMessage::write(&payload).with_address(self.i2c_addr),
        ];
    
        bus.transfer(&mut msgs)?;
        return Ok(());
    }

    fn read_val8(&self, bus : &mut LinuxI2CBus) -> Result<u8, LinuxI2CError> {
        let mut payload  : [u8; 1]= [0; 1];

        let mut msgs : [LinuxI2CMessage; 1] = [
            LinuxI2CMessage::read(&mut payload).with_address(self.i2c_addr),
        ];
        
        bus.transfer(&mut msgs)?;
        return Ok(payload[0]);
    }

    pub fn set_switch<T: Into<u8>> (&self, bus : &mut LinuxI2CBus, switches : T) -> Result<(), LinuxI2CError> {
        self.write_val8(bus, switches.into())?;

        return Ok(());
    }
}
