#![allow(dead_code)]
extern crate structopt;
extern crate pretty_env_logger;
extern crate serde;
#[macro_use] extern crate log;
#[macro_use] extern crate derive_builder;

use std::fs;

use std::path::PathBuf;
use structopt::StructOpt;

use rumqtt::{MqttClient, MqttOptions, QoS};
use std::time::Instant;
use i2cdev::linux::{LinuxI2CBus};


#[path = "drivers/ph.rs"]
mod ph;

#[path = "drivers/tds.rs"]
mod tds;

use tds::adg715::Adg715;
use tds::ad5934::Ad5934;
use tds::cn0349::Cn0349;

use std::string::String;

// MQTT basics

const PUBLISHING_HYSTERESIS_TEMPERATUR : f64 = 0.025;
const PUBLISHING_HYSTERESIS_CONDUCTIVITY : f64 = 0.005;

#[cfg(any(target_os = "linux", target_os = "android"))]
fn main() {
    pretty_env_logger::init();
    let opt = Opt::from_args();

    info!("SPI device{:?}",opt.spi);
    info!("I2C device{:?}",opt.i2c);
    info!("MQTT IP {:?}",opt.mqtt_ip);

    let mqtt_options = MqttOptions::new(&opt.mqtt_client_id, &opt.mqtt_ip, opt.mqtt_port);
    let (mut mqtt_client, _notifications) = MqttClient::start(mqtt_options).expect("Unable to connect to MQTT broker");

    let mut temperature = 0f64;
    let mut conductivity = 0f64;
 
    // MeasureContext, CalibrationPoints, ProbeCalibration, 
    //let pc = drivers::tds::ProbeCalibrationBuilder::default();
    //let mc = drivers::tds::cn0349::MeasureContextBuilder::default();
    //let cp = drivers::tds::CalibrationPointsBuilder::default();

// i2c device 
// - bus    
    let mut i2c_bus = LinuxI2CBus::new(opt.i2c).unwrap();

    let adg715 : Adg715 = Adg715::new();
    let ad5934 : Ad5934 = Ad5934::new();
    let cn0349 : Cn0349 = Cn0349::new(adg715, ad5934);

// Runtime object
// Configuration object used by Runtime, shall be serializable and use only primitive types

// adg715
// - i2c bus
// provides high level register access and associated types

// ad5934
// - i2c bus
// - i2c addr sub-adr
// provides high level register access and associated types

// cn0349
// new::
// - adg715
// - ad5934
// - Measure Context
// - Calibration Points 
// state::
// - calibration config
// - measure context
// - calib points are discarded after calibration
// provides calibrated measuremet

// tds
// - cn0349
// - ProbeCalibration


    // Used for unaveraged sensor sampling frequency calculation
    let mut start = Instant::now();


    //fs::read_to_string(&opt.)?.parse()?;


//    let json: serde_json::Value =
 //   serde_json::from_str(the_file).expect("JSON was not well-formatted");    

    let pc =  tds::ProbeCalibration::default();

    let mut num_measurements = 0;
    loop {
        num_measurements += 1;

        let (new_conductivity, new_temperature, cond, resistance) = tds::measure_compensated_tds_and_temperature(&mut i2c_bus, &pc, &cn0349).unwrap();

        if (new_temperature - temperature).abs() > PUBLISHING_HYSTERESIS_TEMPERATUR {
            temperature = new_temperature;
            mqtt_client.publish(&opt.mqtt_topic_temp, QoS::AtLeastOnce, false, format!("{:1}", temperature)).expect("Error publishing temperature");
        }

        if (new_conductivity - conductivity).abs() > PUBLISHING_HYSTERESIS_CONDUCTIVITY {
            conductivity = new_conductivity;
            mqtt_client.publish(&opt.mqtt_topic_ec, QoS::AtLeastOnce, false, format!("{:1}", conductivity)).expect("Error publishing conductivity");
        }

        let u_volt = ph::ad7793::measure_uv();
        let ph = ph::measure_ph(new_temperature);

        let u_volt = 0.0;
        let ph = 0.0;

        const OBSERVATION_DURATION : u32 = 10;
        if num_measurements % OBSERVATION_DURATION == 0 {
            num_measurements = 0;
            // Adjust elapsed time for observation duration and avg-window and convert to frequency
            let duration = 1f32 / (start.elapsed().as_secs_f32() / (OBSERVATION_DURATION) as f32);
            start = Instant::now();
            info!("Avg. sampling rate: {:?} hz", duration);
        }

        info!(" - tds_mS: {:.2}, mS: {:.2?}, pH: {:.2?}°C: {:.1}, Rtemp: {:.1?}, uV: {:.1?}", new_conductivity, cond, ph, new_temperature, resistance, u_volt / 1.0);
    }
    
    //mqtt_client.subscribe("hello/world", QoS::AtLeastOnce).unwrap();
}


#[derive(StructOpt, Debug)]
#[structopt(name = "basic")]
pub struct Opt {
    #[structopt(short, long, parse(from_occurrences))]
    verbose: u8,

    #[structopt(long, env, parse(from_os_str), default_value = "/dev/spidev0.1")]
    pub spi: PathBuf,

    #[structopt(long, env, parse(from_os_str), default_value = "/dev/i2c-1")]
    pub i2c: PathBuf,

    #[structopt(short = "b", env, long, default_value = "192.168.0.80")]
    pub mqtt_ip: String,

    #[structopt(short = "p", env, long, default_value = "1883")]
    pub mqtt_port : u16,
    
    #[structopt(short = "t", env, long, default_value = "abiyo/sensor/water/temp")]
    pub mqtt_topic_temp: String,

    #[structopt(short = "e", env, long, default_value = "abiyo/sensor/water/ec")]
    pub mqtt_topic_ec: String,

    #[structopt(short = "h", env, long, default_value = "abiyo/sensor/water/ph")]
    pub mqtt_topic_ph: String,

    #[structopt(short = "i", env, long, default_value = "abiyo-sensor-waterquality")]
    pub mqtt_client_id : String,   
}