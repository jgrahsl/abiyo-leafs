
pub fn scan_all_buses() {
    const BusNums : [u8; 4] = [0, 1, 2, 4];

    let mut paths : Vec<String> = Vec::new();
    
    for i in &BusNums {
        let s = format!("/dev/i2c-{}", i);
        paths.push(s);
    }

    for i in paths {
        let mut bus = match LinuxI2CBus::new(&i) {
            Ok(bus) => bus,
            Err(_e) => {
                println!("Error opening I2C Bus {} {}", &i, _e);
                return;
            }
        };
        
        println!("Opened I2C Bus OK: {}", &i);
    
        let mut data = [0; 1];
        for x in 1..128 {
            let msg = LinuxI2CMessage::read(&mut data).with_address(x);
    
            match bus.transfer(&mut [msg]) {
                Ok(_rc) => println!("Found 7-bit addr 0x{:02X}", x),
                Err(_e) => {}
            }
        }
    }
}

pub fn read_nvidia_eeprom() {
    let mut eeprom_bus = match LinuxI2CBus::new("/dev/i2c-2") {
        Ok(bus) => bus,
        Err(_e) => {
            println!("Error opening EEPROM I2C Bus");
            return;
        }
    };

    let mut eeprom_data : [u8; 256] = [0; 256];
    let set_adr_command : [u8; 1] = [0x00];
    let set_adr = LinuxI2CMessage::write(&set_adr_command).with_address(0x57);
    let read_data = LinuxI2CMessage::read(&mut eeprom_data).with_address(0x57);

    match eeprom_bus.transfer(&mut [set_adr, read_data]) {
        Ok(_rc) => println!("Sucessfull read"),
        Err(_e) => {}
    }

    println!("{}", 1);
    for i in 0..256 {

        let ch : AsciiChar = match ascii::AsciiChar::from_ascii(eeprom_data[i]) {
            Ok(va) => va,
            Err(_) => {AsciiChar::Dot}
        };

        print!("{}", ch); // , (eeprom_data[i] & 0x7f_u8));
        if (i + 1) % 16 == 0 {
            println!("");
        } else {
            print!(" ");            
        }
    }

}
