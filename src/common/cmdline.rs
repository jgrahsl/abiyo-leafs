extern crate structopt;

use std::path::PathBuf;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "basic")]
pub struct Opt {
    // A flag, true if used in the command line. Note doc comment will
    // be used for the help message of the flag. The name of the
    // argument will be, by default, based on the name of the field.
    /// Activate debug mode
    /// 
    // #[structopt(short, long)]
    // debug: bool,

    // The number of occurrences of the `v/verbose` flag
    // Verbose mode (-v, -vv, -vvv, etc.)
    #[structopt(short, long, parse(from_occurrences))]
    verbose: u8,

    // Set speed
    // 
    // #[structopt(short, long, default_value = "42")]
    //speed: f64,

    // Output file
    // 
    // #[structopt(short, long, parse(from_os_str))]
    // output: PathBuf,

    // the long option will be translated by default to kebab case,
    // i.e. `--nb-cars`.
    // Number of cars
    // 
    // #[structopt(short = "c", long)]
    // nb_cars: Option<i32>,

    // admin_level to consider
    // 
    // #[structopt(short, long)]
    // level: Vec<String>,

    // Files to process
    // #[structopt(name = "FILE", parse(from_os_str))]
    // files: Vec<PathBuf>,

    #[structopt(long, env, parse(from_os_str), default_value = "/dev/spidev0.1")]
    pub spi: PathBuf,

    #[structopt(long, env, parse(from_os_str), default_value = "/dev/i2c-1")]
    pub i2c: PathBuf,

    #[structopt(short, env, long, default_value = "10.0.0.70")]
    pub mqtt: String,

     #[structopt(long, env, parse(from_os_str), default_value = "probe_calib.json")]
    pub cal: PathBuf,
}