SHELL := /bin/bash

# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help build shell run
.DEFAULT_GOAL := help

.EXPORT_ALL_VARIABLES:
DISPLAY ?=$(shell w $$(id -un) | awk 'NF > 7 && $$2 ~ /tty[0-9]+/ {print $$3; exit}')
SELENIUM_HOST ?=

ARCH=$(shell arch)

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

login:
	docker login cr.grahsl.net

build: login
	docker-compose run build

dev: login
	docker-compose run dev

run: login
	docker-compose run run

test: login
	docker-compose run test

install: build
	cp target/debug/abiyo-sensor-conductivity /usr/bin/
